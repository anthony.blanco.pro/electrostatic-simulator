#include <gtk/gtk.h>
#include <glib.h>

#include "src/main.h"

INT main (INT argc, STRING argv[]) {
    /* Variables */
    GtkApplication *GTKApp;
    INT GTKAppStatus;

    /* App ID */
    GTKApp = gtk_application_new("fr.prjisen.ess", G_APPLICATION_FLAGS_NONE);
    
    /* App Events */
    g_signal_connect(GTKApp, "activate", G_CALLBACK(ESSAppMain), NULL);
    
    /* Run App */
    GTKAppStatus = g_application_run(G_APPLICATION(GTKApp), argc, argv);
    g_object_unref(GTKApp);
    
    return GTKAppStatus;
}