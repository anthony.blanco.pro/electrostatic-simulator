# ElectroMagneticSimulator

## Installation 

Voici le manuel d'utilisation de notre projet : <https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/blob/dev/ManuelUtilisation.md>

Pour utiliser l'application correctement il ya plusieurs commandes à savoir : 


| Commande      | Action                                                                                    |
|---------------|-------------------------------------------------------------------------------------------|
| make          | Compile l'application.                                                                    |
| make sync     | Synchronise les sub-module.                                                               |
| make launch   | Compile et lance l'application.                                                           |
| make unit     | Lance les tests unitaires.                                                                |
| make mrproper | Supprime tous les fichiers générés.                                                       |
| make clean    | Supprime tous les fichiers générés dans l'application présente sauf le fichier de config. |

Pour utiliser l'application veuillez effectuer les commandes suivantes : 

```bash
git clone <https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator.git>
cd electrostatic-simulator
make sync && make launch
```

## Sommaire

1. [Introduction](#Introduction)

2. [App](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/dev/src/app)

3. [Charge](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/dev/src/charge)

4. [Display](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/dev/src/display)

5. [Drawer](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/dev/src/drawer)

6. [Events](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/24-ess-documentation/src/events)

7. [Popups](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/24-ess-documentation/src/popups)

8. [Random](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/dev/src/random)

9. [Simulation](https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator/-/tree/24-ess-documentation/src/simulation)

10. [Main](#main)

## Introduction <a name="Introduction"></a>

Electrostatic-simulator est notre application qui permet de faire la simulation.

Il est composé de plusieurs dossiers afin d'organiser le module. On y retrouve :

| Dossier | Description                                                           | Généré lors de la compilation |
|---------|-----------------------------------------------------------------------|-------------------------------|
| bin     | Contient le module elecsim.a                                          | Oui                           |
| build   | Contient tous les fichiers objets des sources                         | Oui                           |
| libs    | Contient les deux librairies math et physique.                        |                               |
| src     | Contient tous les fichiers source avec les fonctionnalitées du module |                               |
| ui      | Contient les fichiers .glade ainsi que les images des boutons.        |                               |
| utils   | Contient tous les fichiers utiles, notamment les MACROS.              |                               |

Le fichier utils/MACRO_REF.H est un fichier repertoriant tous les macros génériques du module. Elle comprends :

| Macro            | Fonction                      | Description                                             |
|------------------|-------------------------------|---------------------------------------------------------|
| NEW(type)        | ((type*)malloc(sizeof(type))) | Allouer un espace mémoire de la taille de 'type'        |
| DELETE(ptr)      | (free(ptr))                   | Dé-allouer l'espace mémoire d'un pointer                |
| NULL             | (void*)0                      | Pointer vers 'rien'                                     |
| FUNC(fnc)        | (*fnc)                        | Simplifier l'écriture d'une fonction dans une structure |
| CASTV(var, type) | (*(type*)var)                 | Convertir le type d'une variable en un autre type       |
| UNUSED(x)        | (VOID*)(x)                    | Convertir une variable en pointer                       |
| FALSE            | 0                             | Définir FALSE comme étant une entier égale à 0          |
| TRUE             | 1                             | Définir TRUE comme étant une entier égale à 0           |

Il comprend également plusieurs alias (typedef) comme :

| Type                         | Alias                      |
|------------------------------|----------------------------|
| void                         | VOID                       |
| int                          | INT                        |
| char                         | CHAR                       |
| short                        | SHORT                      |
| float                        | FLOAT                      |
| long                         | LONG                       |
| double                       | DOUBLE                     |
| CHAR*                        | STRING                     |
| UCHAR                        | BYTE                       |
| gboolean                     | BOOL                       |
| BYTE                         | BOOLEAN                    |
| signed char/short/int/long   | INT8/INT16/INT32/INT64     |
| unsigned char/short/int/long | UINT8/UINT16/UINT32/UINT64 |
| unsigned char/short/int/long | UCHAR/USHORT/UINT/ULONG    |

Le fichier utils/RGB.h contient une structure _RGB_ qui contient 3 doubles pour les 3 couleurs Rouge, Vert, Bleu.

Le dossier **ui** contient nos fichiers _.glade_ qui sont les différentes interfaces graphiques de notre simulation ainsi que les images qui sont contenu dans les différents boutons :

| Fichier                 | Contenu                                                                |
|-------------------------|------------------------------------------------------------------------|
| ess_gui.glade           | Contient toute l'interface du projet.                                  |
| ess_popup_charges.glade | Contient l'interface de la popup pour choisir un proton ou un électron |
| ess_popup_options.glade | Contient l'interface de la popUp des options.                          |


## Main <a name="main"></a>

| Type | Fonction          | Description                                   |
|------|-------------------|-----------------------------------------------|
| VOID | ESSAppInitWidgets | Initialise tous les widgets de l'application. |
| VOID | ESSAppMain        | Main de l'application.                        |
