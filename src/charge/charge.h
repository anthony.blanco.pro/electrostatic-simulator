/* File format.h */
/* Date: 2020-06-08 */
/* Description: charge interface */

/* ========== Includes ========== */

#include "../app/app.h"
#include "../drawer/draw.h"
#include "../display/display.h"
#include "../popups/popupCharges.h"

/* ========== Prototype of functions ========== */


VOID        change_label_size       (App *ESSApp);                                                                                                        /* Change label size of list */
VOID        change_label_info       (App *ESSApp, GtkLabel* labelIdx, GtkLabel* labelPosX,  GtkLabel* labelPosY, INT idx, DOUBLE posX, DOUBLE posY);    /* Chnage label for charge */
VOID        add_label_charge_info   (App *ESSApp, GtkLabel *labelIdx, GtkLabel* labelPosX,  GtkLabel* labelPosY, DOUBLE posX, DOUBLE posY);             /* Add label for charge */
VOID        add_charge_on_the_grid  (App *ESSApp, GtkWidget *GWidget, DOUBLE posX, DOUBLE posY, BOOLEAN isFixe);                                        /* Add charge with window coordinates */
AppCharge   *searchCharge           (App *ESSApp, DOUBLE cursorX, DOUBLE cursorY, INT *idx);                                                            /*  */
