/* File charge.h */
/* Date: 2020-06-08 */
/* Description: charge interface */

#include "charge.h"

/*
  Function: change_label_size
  ----------------
  Description: Change label size of list
  Return: 
 */
VOID change_label_size(App*ESSApp){

	CHAR arr[20];

	sprintf(arr, "[%d]", ESSApp->charges->length);
	gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 17)), arr);

}
/*
  Function: change_label_info
  ----------------
  Description: change label for charge
  Return: 
 */
VOID change_label_info(App *ESSApp, GtkLabel* labelIdx, GtkLabel* labelPosX,  GtkLabel* labelPosY, INT idx, DOUBLE posX, DOUBLE posY){
	CHAR arr[20];

	change_label_size(ESSApp);

	if(labelIdx != NULL){
	
		sprintf(arr, "[%d]", idx);
		gtk_label_set_text(labelIdx, arr);
		gtk_label_set_xalign(labelIdx, 0);	
	}

	if (abs(posX) < 1000) {
	  sprintf(arr, "x = %.lf", posX);
	} else {
	  sprintf(arr, "x = %.2e", posX);
	}
	gtk_label_set_text(labelPosX, arr);
	gtk_label_set_xalign(labelPosX, 0);	
	
	if (abs(posY) < 1000) {
	  sprintf(arr, "y = %.lf", posY);
	} else {
	  sprintf(arr, "y = %.2e", posY);
	}
	gtk_label_set_text(labelPosY, arr);
	gtk_label_set_xalign(labelPosY, 0);

}

/*
  Function: add_label_charge_info
  ----------------
  Description: add label for charge
  Return: 
 */
VOID add_label_charge_info(App *ESSApp, GtkLabel *labelIdx, GtkLabel* labelPosX, GtkLabel* labelPosY, DOUBLE posX, DOUBLE posY) {
	change_label_info(ESSApp, labelIdx, labelPosX, labelPosY, ESSApp->charges->length, posX, posY);
	
	GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	gtk_container_add(GTK_CONTAINER(box), GTK_WIDGET(labelIdx));
	gtk_container_add(GTK_CONTAINER(box), GTK_WIDGET(labelPosX));
	gtk_container_add(GTK_CONTAINER(box), GTK_WIDGET(labelPosY));
	gtk_container_add(GTK_CONTAINER(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 18)), box);

	gtk_widget_show_all(box);

}

/*
  Function: add_charge_on_the_grid
  ----------------
  Description:  add charge with window coordinates
  Return: 
 */
VOID add_charge_on_the_grid(App *ESSApp, GtkWidget *GWidget, DOUBLE posX, DOUBLE posY, BOOLEAN isFixe) {

	DOUBLE posChgX = (DOUBLE)((INT)((-ESSApp->options->posAxeX + posX) / ESSApp->options->zoom));
	DOUBLE posChgY = (DOUBLE)((INT)((ESSApp->options->posAxeY - posY) / ESSApp->options->zoom));

	Charge *chg = newCharge(newPoint(posChgX, posChgY, 0), 0, isFixe);

	GtkLabel *labelIdx = GTK_LABEL(gtk_label_new(""));
	GtkLabel *labelPosX = GTK_LABEL(gtk_label_new(""));
	GtkLabel *labelPosY = GTK_LABEL(gtk_label_new(""));
	
	AppCharge *appChg = newAppCharge(chg, labelIdx, labelPosX, labelPosY);
	ESSApp->charges->Append(ESSApp->charges, appChg);

	add_label_charge_info(ESSApp, labelIdx, labelPosX, labelPosY, posChgX, posChgY);

	on_popup_charges_clicked(GWidget, ESSApp);
	return;

}

/*
  Function: searchCharge
  ----------------
  Description: search nearby at cursor coordinates
  Return: AppCharge
 */
AppCharge *searchCharge(App *ESSApp, DOUBLE cursorX, DOUBLE cursorY, INT *idx) {
    DOUBLE posChgX;
    DOUBLE posChgY;
    INT size;
    
    AppCharge *appChg;
    BOOLEAN onCharge = FALSE;
    
    while (!onCharge && *idx != -1 && ESSApp->charges->length != 0) {
        appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, *idx);
        posChgX = appChg->chg->coord->x;
        posChgY = appChg->chg->coord->y;
        if ((cursorX > posChgX - _ESS_CHARGE_RADIUS_ * ESSApp->options->zoom &&
				cursorX < posChgX + _ESS_CHARGE_RADIUS_ * ESSApp->options->zoom) &&
				(cursorY > posChgY - _ESS_CHARGE_RADIUS_ * ESSApp->options->zoom &&
				cursorY < posChgY + _ESS_CHARGE_RADIUS_ * ESSApp->options->zoom)) {
            onCharge = TRUE;
        }
        (*idx)--;
    }
	return onCharge ? appChg : NULL;
}
