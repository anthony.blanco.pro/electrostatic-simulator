# Random

1. [Introduction](#Introduction)

2. [Random](#Random)


## Introduction <a name="Introduction"></a>

Le dossier **random** contient les fichiers _random_ qui contiennent les fonctions permettant de renvoyer un nombre en 0 et 1.

## Random <a name="Random"></a>

| Type   | Fonction        | Description                               |
|--------|-----------------|-------------------------------------------|
| DOUBLE | getRandomNumber | Renvoie un nombre aléatoire entre 0 et 1. |