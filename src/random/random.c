/* File random.c */
/* Date: 2020-06-20 */
/* Description: Random file */

#include <stdlib.h>
#include "random.h"

DOUBLE getRandomNumber(){
    return (DOUBLE)rand() / RAND_MAX;
}
