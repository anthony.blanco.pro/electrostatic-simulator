/* File random.h */
/* Date: 2020-06-20 */
/* Description: Random file */

#include <stdlib.h>
#include <time.h>

#include "../../utils/MACRO_REF.h"

DOUBLE          getRandomNumber();          /* Return random double between 0 and 1 */