/* File eventsGrid.c */
/* Date: 2020-06-17 */
/* Description: Events Grid */

#include "eventsGrid.h"

/*
	Function: on_event_grid_zoom_wheel
	----------------
	Description: Zoom in or out with mouse wheel
	Return: gboolean
 */
gboolean on_event_grid_zoom_wheel(GtkWidget *widget, GdkEvent *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	DOUBLE deltaX, deltaY;
	gdk_event_get_scroll_deltas(event, &deltaX, &deltaY);
	
	if (deltaY < 0) {
		/* scroll down */
		ESSApp->options->zoom *= _ESS_WINDOW_ZOOM_COEF_;
		if (ESSApp->options->zoomGraphic <= 200) {
			ESSApp->options->zoomGraphic *= _ESS_WINDOW_ZOOM_COEF_;
		} else {
			ESSApp->options->zoomGraphic = 6;
		}
	} else if (deltaY > 0) {
		/* scroll up */
		ESSApp->options->zoom /= _ESS_WINDOW_ZOOM_COEF_;
		if (ESSApp->options->zoomGraphic >= 6) {
			ESSApp->options->zoomGraphic /= _ESS_WINDOW_ZOOM_COEF_;
		} else {
			ESSApp->options->zoomGraphic = 200;
		}
	}

	ESSApp->options->posAxeX = ESSApp->options->areaWidth / 2;
	ESSApp->options->posAxeY = ESSApp->options->areaHeight / 2;
	gtk_widget_queue_draw(widget);
	
	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_move
	----------------
	Description: Move the grid area
	Return: gboolean
 */
gboolean on_event_grid_move(GtkWidget *widget, GdkEvent *event, App *ESSApp){

	if (ESSApp->isLocked || ESSApp->simStatus == RUNING) {
		return GDK_EVENT_PROPAGATE;
	}
	
	if (event->type == GDK_MOTION_NOTIFY && ESSApp->mode == DEFAULT) {
		GdkEventMotion *e = (GdkEventMotion *)event;
		if (e->state & GDK_BUTTON1_MASK) {
			if (ESSApp->selectedCharge == NULL) {	
				ESSApp->options->posAxeX -= (ESSApp->options->posX - e->x) / 2;
				ESSApp->options->posAxeY -= (ESSApp->options->posY - e->y) / 2;
				ESSApp->options->posX = e->x;
				ESSApp->options->posY = e->y;
			}
			gtk_widget_queue_draw(widget);
		}
	}

	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_move_charge
	----------------
	Description: Move charge on the grid area
	Return: gboolean
 */
gboolean on_event_grid_move_charge(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked || ESSApp->simStatus == RUNING) {
		return GDK_EVENT_PROPAGATE;
	}

	if (event->type == GDK_MOTION_NOTIFY && ESSApp->mode == DEFAULT) {
		GdkEventMotion *e = (GdkEventMotion *)event;
		DOUBLE cursorX = (-ESSApp->options->posAxeX + e->x) / ESSApp->options->zoom;
		DOUBLE cursorY = (ESSApp->options->posAxeY - e->y) / ESSApp->options->zoom;
		INT idx = ESSApp->charges->length - 1;
		
		if (ESSApp->selectedCharge == NULL) {
			ESSApp->selectedCharge = searchCharge(ESSApp, cursorX, cursorY, &idx);
		}

		if (e->state & GDK_BUTTON1_MASK) {
			/* if press button primary when move mouse */
			if (ESSApp->selectedCharge != NULL) {
				ESSApp->selectedCharge->chg->coord->x = cursorX;
				ESSApp->selectedCharge->chg->coord->y = cursorY;
				change_label_info(ESSApp, NULL, ESSApp->selectedCharge->posX, ESSApp->selectedCharge->posY, 0, cursorX, cursorY);
				gtk_widget_queue_draw(widget);
			}
		}
	 }

	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_charge_add_fixe
	----------------
	Description: Add a fixe charge on the grid area
	Followed from : on_event_menu_charge_add_fixe (<- Call this event before)
	Return: gboolean
 */
gboolean on_event_grid_charge_add_fixe(GtkWidget *GWidget, GdkEventButton *event, App *ESSApp) {
	
	if (ESSApp->isLocked || (ESSApp->mode != CHARGE_INSERT_FIXE && ESSApp->mode != CHARGE_INSERT_FIXE_RAND)) {
		return GDK_EVENT_PROPAGATE;
	}
	
	ESSApp->isLocked = TRUE;
	if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		add_charge_on_the_grid(ESSApp, GWidget, event->x, event->y, TRUE);
		gtk_widget_queue_draw(GWidget);
	}
	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_charge_add_mobile
	----------------
	Description: Add a mobile charge on the grid area
	Followed from : on_event_menu_charge_add_mobile (<- Call this event before)
	Return: gboolean
 */
gboolean on_event_grid_charge_add_mobile(GtkWidget *GWidget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked || (ESSApp->mode != CHARGE_INSERT_MOBILE && ESSApp->mode != CHARGE_INSERT_MOBILE_RAND)) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->isLocked = TRUE;

	if (event->type == GDK_BUTTON_PRESS && event->button == 1) {
		add_charge_on_the_grid(ESSApp, GWidget, event->x, event->y, FALSE);
		gtk_widget_queue_draw(GWidget);
	}
	
	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_charge_delete
	----------------
	Description: Delete a charge on the grid area
	Followed from : on_event_menu_charge_delete (<- Call this event before)
	Return: gboolean
 */
gboolean on_event_grid_charge_delete(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {
	
	if (ESSApp->isLocked || ESSApp->simStatus == RUNING) {
		return GDK_EVENT_PROPAGATE;
	}

	if(event->type == GDK_BUTTON_PRESS && ESSApp->mode == CHARGE_DELETE) {

		DOUBLE cursorX = (-ESSApp->options->posAxeX + event->x) / ESSApp->options->zoom;
		DOUBLE cursorY = (ESSApp->options->posAxeY - event->y) / ESSApp->options->zoom;
		INT idx = ESSApp->charges->length - 1;
		AppCharge* appChg = searchCharge(ESSApp, cursorX, cursorY, &idx);
		
		if(appChg != NULL){
			gtk_widget_destroy(gtk_widget_get_parent(GTK_WIDGET(appChg->posX)));
			ESSApp->charges->DeleteByIndex(ESSApp->charges, idx + 1);
		}
		ESSApp->selectedCharge = NULL;

		for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
			appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
			change_label_info(ESSApp, appChg->idx, appChg->posX, appChg->posY, idx + 1 , appChg->chg->coord->x, appChg->chg->coord->y);
		}
		

		change_label_size(ESSApp);
		gtk_widget_queue_draw(widget);
		
	}
	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_grid_click
	----------------
	Description: Do something on grid click depending on the status
	Return: gboolean
 */
gboolean on_event_grid_click(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	ESSApp->options->posX = event->x;
	ESSApp->options->posY = event->y;

	if (ESSApp->mode != DEFAULT && event->type == GDK_BUTTON_PRESS && event->button == 3) {
		set_cursor_default(ESSApp, TRUE, TRUE, TRUE);
	}

	DOUBLE cursorX = (-ESSApp->options->posAxeX + event->x) / ESSApp->options->zoom;
	DOUBLE cursorY = (ESSApp->options->posAxeY - event->y) / ESSApp->options->zoom;
	INT idx = ESSApp->charges->length - 1;
	ESSApp->selectedCharge = searchCharge(ESSApp, cursorX, cursorY, &idx);
	gtk_widget_queue_draw(widget);
	
	return GDK_EVENT_PROPAGATE;

}