/* File eventsBtn.h */
/* Date: 2020-06-17 */
/* Description: Buttons Events */

#include <gtk/gtk.h>
#include <cairo.h>

#include "../app/app.h"
#include "../drawer/draw.h"
#include "../display/display.h"

gboolean    on_event_tools_draw                     (GtkWidget *widget, cairo_t *cr, App *app);                     /* Event [TOOLS_DRAW] - Draw the grid */
gboolean    on_event_tools_move_window              (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [TOOLS_WINDOW] - Move window */
gboolean    on_event_tools_cursor_position          (GtkWidget *widget, GdkEvent *event, App *ESSApp);              /* Event [TOOLS_CURSOR_POSITION] - Show cursor position */
gboolean    on_event_tools_cursor_measurement       (GtkWidget *widget, GdkEvent *event, App *ESSApp);              /* Event [TOOLS_CURSOR_MEASUREMENT] - Show measurements */
