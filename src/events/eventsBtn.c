/* File eventsMenu.c */
/* Date: 2020-06-17 */
/* Description: Menue Events */

#include "eventsBtn.h"

/*
    Function: on_event_grid_center
    ----------------
    Description: Center the grid area
    Return: gboolean
 */
gboolean on_event_btn_center(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->options->posAxeX = ESSApp->options->areaWidth / 2;
	ESSApp->options->posAxeY = ESSApp->options->areaHeight / 2;
	gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_grid_zoom_in
    ----------------
    Description: Zoom in the grid area
    Return: gboolean
 */
gboolean on_event_btn_zoom_in(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->options->zoom *= _ESS_WINDOW_ZOOM_COEF_;
	if (ESSApp->options->zoomGraphic <= 200) {
		ESSApp->options->zoomGraphic *= _ESS_WINDOW_ZOOM_COEF_;
	} else {
		ESSApp->options->zoomGraphic = 6;
	}

	gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));
    
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_grid_zoom_out
    ----------------
    Description: Zoom out the grid area
    Return: gboolean
 */
gboolean on_event_btn_zoom_out(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->options->zoom /= _ESS_WINDOW_ZOOM_COEF_;
	if (ESSApp->options->zoomGraphic >= 6) {
		ESSApp->options->zoomGraphic /= _ESS_WINDOW_ZOOM_COEF_;
	} else {
		ESSApp->options->zoomGraphic = 200;
	}
	gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));
    
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_reset
    ----------------
    Description: Reset the simulation, all charges and center the grid
    Return: gboolean
 */
gboolean on_event_btn_reset(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	AppCharge *appChg;
	GtkWidget *box;

	for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
		appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
		box = gtk_widget_get_parent(GTK_WIDGET(appChg->posX));
		gtk_widget_destroy(box);
	}
	DELETE(ESSApp->charges);
	ESSApp->charges = newList();
	ESSApp->simStatus = STOPED;
	change_label_size(ESSApp);
	on_event_btn_center(widget, event, ESSApp);
    
    return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_cursor_measurement
    ----------------
    Description: Change cursor measurement 
    Return: gboolean
 */
gboolean on_event_btn_cursor_measurement(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->mode = MEASUREMENT;
	set_cursor(widget, GDK_CROSS);

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_cursor_default
    ----------------
    Description: 
    Return: gboolean
 */
gboolean on_event_btn_cursor_default(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	ESSApp->selectedCharge = NULL;
	ESSApp->isLocked = FALSE;
	ESSApp->mode = DEFAULT;
	set_cursor_default(ESSApp, TRUE, TRUE, TRUE);
	gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_help
    ----------------
    Description: Open settings window
    Return: gboolean
 */
gboolean on_event_btn_help(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}
	
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_settings
    ----------------
    Description: Open settings window
    Return: gboolean
 */
gboolean on_event_btn_settings(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	ESSApp->simStatus = PAUSED;
	changeBtnStatus(ESSApp);

	set_cursor_default(ESSApp, TRUE, TRUE, TRUE);
	on_popup_settings_clicked(widget, ESSApp);

	return GDK_EVENT_PROPAGATE;

}


/*
    Function: on_event_btn_charge_add_fixe
    ----------------
    Description: Add fixe charge on the grid are. Dble click create a random fixe charge
    Return: gboolean
 */
gboolean on_event_btn_charge_add_fixe(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {
	
	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if (event->type == GDK_2BUTTON_PRESS) {
		DOUBLE randPosX = getRandomNumber() * ESSApp->options->areaWidth;
		DOUBLE randPosY = getRandomNumber() * ESSApp->options->areaHeight;
		set_cursor_default(ESSApp, TRUE, TRUE, TRUE);
		add_charge_on_the_grid(ESSApp, widget, randPosX, randPosY, TRUE);
	} else if (GDK_BUTTON_PRESS) {
		set_cursor(widget, GDK_PENCIL);
		ESSApp->mode = CHARGE_INSERT_FIXE;
	} 
	
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_charge_add_mobile
    ----------------
    Description: Add mobile charge on the grid are. Dble click create a random mobile charge
    Return: gboolean
 */
gboolean on_event_btn_charge_add_mobile(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if (event->type == GDK_2BUTTON_PRESS) {
		DOUBLE randPosX = getRandomNumber() * ESSApp->options->areaWidth;
		DOUBLE randPosY = getRandomNumber() * ESSApp->options->areaHeight;
		set_cursor_default(ESSApp, TRUE, TRUE, TRUE);
		add_charge_on_the_grid(ESSApp, widget, randPosX, randPosY, FALSE);
	} else if (GDK_BUTTON_PRESS) {
		set_cursor(widget, GDK_PENCIL);
		ESSApp->mode = CHARGE_INSERT_MOBILE;
	} 
	
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_charge_add_random
    ----------------
    Description: Add random charge on the grid area
    Return: gboolean
 */
gboolean on_event_btn_charge_add_random(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if(event->type == GDK_BUTTON_PRESS){

		set_cursor_default(ESSApp, TRUE, TRUE, TRUE);

		BOOLEAN isNegativeChrg = getRandomNumber() < 0.5;
		BOOLEAN isFixeChrg = getRandomNumber() < 0.5;

		DOUBLE randPosX =  getRandomNumber() * ESSApp->options->areaWidth;
		DOUBLE randPosY = getRandomNumber() * ESSApp->options->areaHeight;
		DOUBLE posChgX = (DOUBLE)((INT)((-ESSApp->options->posAxeX + randPosX) / ESSApp->options->zoom));
		DOUBLE posChgY = (DOUBLE)((INT)((ESSApp->options->posAxeY - randPosY) / ESSApp->options->zoom));

		Charge *chg = newCharge(newPoint(posChgX, posChgY, 0), 0, isFixeChrg);
		GtkLabel *labelIdx = GTK_LABEL(gtk_label_new(""));
		GtkLabel *labelPosX = GTK_LABEL(gtk_label_new(""));
		GtkLabel *labelPosY = GTK_LABEL(gtk_label_new(""));

		AppCharge *appChg = newAppCharge(chg, labelIdx, labelPosX, labelPosY);
		ESSApp->charges->Append(ESSApp->charges, appChg);

		add_label_charge_info(ESSApp, labelIdx, labelPosX, labelPosY, posChgX, posChgY);
		
		appChg->chg->chgq = 1.602E-19;
		appChg->chg->chgq = isNegativeChrg ? -appChg->chg->chgq : appChg->chg->chgq;

		gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

	}
	
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_charge_delete
    ----------------
    Description: 
    Return: gboolean
 */
gboolean on_event_btn_charge_delete(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {
	
	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}
	ESSApp->mode = CHARGE_DELETE;
	set_cursor(widget, GDK_CROSS_REVERSE);	

    return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_start_stop
    ----------------
    Description: Start the simulation
    Return: gboolean
 */
gboolean on_event_btn_play(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}
	
	if(ESSApp->saveCharges == NULL){
        ESSApp->saveCharges = newList();
        saveListBeforeSimulation(ESSApp, ESSApp->saveCharges);
	}

    if ((ESSApp->simStatus == STOPED || ESSApp->simStatus == PAUSED) && ESSApp->charges->length > 1) {
        ESSApp->simStatus = RUNING;
		changeBtnStatus(ESSApp);
        g_timeout_add(50, simulation, ESSApp);
    }

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_pause
    ----------------
    Description: Pause the simulation
    Return: gboolean
 */
gboolean on_event_btn_pause(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if (ESSApp->simStatus == RUNING) {
        ESSApp->simStatus = PAUSED;
		changeBtnStatus(ESSApp);
    }

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_stop
    ----------------
    Description: Pause the simulation
    Return: gboolean
 */
gboolean on_event_btn_stop(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if(ESSApp->saveCharges != NULL){
		ESSApp->simStatus = STOPED;
		changeBtnStatus(ESSApp);
		ESSApp->options->simTimeStart = 0;
		DELETE(ESSApp->charges);
		ESSApp->charges = NULL;
		ESSApp->charges = ESSApp->saveCharges;
		ESSApp->saveCharges = NULL;
		AppCharge* appChg;
		for (INT idx = 0; idx < ESSApp->charges->length; idx++)
		{
			appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
			change_label_info(ESSApp, appChg->idx, appChg->posX, appChg->posY, idx, appChg->chg->coord->x, appChg->chg->coord->y);
		}
		gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

	}

	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_replay
    ----------------
    Description: Replay the simulation
    Return: gboolean
 */
gboolean on_event_btn_replay(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if (ESSApp->isLocked) {
		return GDK_EVENT_PROPAGATE;
	}

	if(ESSApp->saveCharges != NULL){

		ESSApp->options->simTimeStart = 0;
		DELETE(ESSApp->charges);
		ESSApp->charges = ESSApp->saveCharges;
		ESSApp->saveCharges = NULL;
		
		AppCharge* appChg;
		for (INT idx = 0; idx < ESSApp->charges->length; idx++) {
			appChg = ESSApp->charges->GetDataByIndex(ESSApp->charges, idx);
			change_label_info(ESSApp, appChg->idx, appChg->posX, appChg->posY, idx, appChg->chg->coord->x, appChg->chg->coord->y);
		}

		gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

		on_event_btn_play(widget, event, ESSApp);		

	}

	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_btn_speedup
	----------------
	Description: Speed up the time of the simulation
	Return: gboolean
 */
gboolean on_event_btn_speedup(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {
	
	loggerAddLog(ESSApp->logger, DEBUG, "Speed UP");
	ESSApp->options->simTimeStart /= _ESS_SIMULATION_SPEED_COEF_;
	ESSApp->options->simTimeIncrement /= _ESS_SIMULATION_SPEED_COEF_;
	gtk_widget_queue_draw(ESSApp->widgets->first->data);
	return GDK_EVENT_PROPAGATE;

}

/*
	Function: on_event_btn_speeddown
	----------------
	Description: Speed down the time of the simulation
	Return: gboolean
 */
gboolean on_event_btn_speeddown(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {
	
	loggerAddLog(ESSApp->logger, DEBUG, "Speed DOWN");
	ESSApp->options->simTimeStart *= _ESS_SIMULATION_SPEED_COEF_;
	ESSApp->options->simTimeIncrement *= _ESS_SIMULATION_SPEED_COEF_;
	gtk_widget_queue_draw(ESSApp->widgets->first->data);
	return GDK_EVENT_PROPAGATE;

}

/*
    Function: on_event_btn_charge_edit
    ----------------
    Description: Edit selected charge
    Return: gboolean
 */
gboolean on_event_btn_charge_edit(GtkWidget *widget, GdkEventButton *event, App *ESSApp) {

	if(ESSApp->selectedCharge != NULL) {
		
		ESSApp->simStatus = STOPED;
		on_popup_edit_clicked(widget, ESSApp);
		set_cursor_default(ESSApp, TRUE, TRUE, FALSE);
		gtk_widget_queue_draw(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 1));

	}

	return GDK_EVENT_PROPAGATE;

}
