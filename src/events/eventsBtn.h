/* File eventsMenu.h */
/* Date: 2020-06-17 */
/* Description: Events Menu objects */

#include "../app/app.h"
#include "../charge/charge.h"
#include "../display/display.h"
#include "../popups/popupCharges.h"
#include "../popups/popupSettings.h"
#include "../popups/popupEdit.h"
#include "../simulation/simulation.h"

gboolean    on_event_btn_center                 (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CENTER] - Center grid */
gboolean    on_event_btn_zoom_in                (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_ZOOM_IN] - Zoom in the grid */
gboolean    on_event_btn_zoom_out               (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_ZOOM_OUT] - Zoom out the grid */
gboolean    on_event_btn_reset                  (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_RESET] - reset all */
gboolean    on_event_btn_cursor_measurement     (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CURSOR_MEASUREMENT] - change cursor measurement */
gboolean    on_event_btn_cursor_default         (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CURSOR_DEFAULT] - when click change cursor default */
gboolean    on_event_btn_settings               (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SELECT_OPTIONS] - options window for configuration */
gboolean    on_event_btn_help                   (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SELECT_HELP] - help button */
gboolean    on_event_btn_charge_add_fixe        (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CHARGE_ADD_FIXE] - add fixe charge */
gboolean    on_event_btn_charge_add_mobile      (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CHARGE_ADD_MOBILE] - add mobile charge */
gboolean    on_event_btn_charge_add_random      (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CHARGE_ADD_RANDOM] - add mobile charge */
gboolean    on_event_btn_charge_delete          (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CHARGE_DELETE] - delte charge */
gboolean    on_event_btn_play                   (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_PLAY] - Play the simulation */
gboolean    on_event_btn_pause                  (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_PAUSE] - Pause the simulation */
gboolean    on_event_btn_stop                   (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_STOP] - Stop the simulation */
gboolean    on_event_btn_replay                 (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_REPLAY] - Replay or stop the simulation */
gboolean    on_event_btn_speedup                (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_SPEEDUP] - speed up the simulation */
gboolean    on_event_btn_speeddown              (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_SIMULATION_SPEEDDOWN] - speed down the simulation */
gboolean    on_event_btn_charge_edit            (GtkWidget *widget, GdkEventButton *event, App *ESSApp);        /* Event [MENU_CHARGE_EDIT] - Edit selected charge */
