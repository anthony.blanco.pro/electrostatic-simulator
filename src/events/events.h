/* File events.c */
/* Date: 2020-06-17 */
/* Description: Event Manager */

/* ========== Includes ========== */

#include <gtk/gtk.h>
#include <cairo.h>

#include "../app/app.h"

#include "eventsGrid.h"
#include "eventsTools.h"
#include "eventsBtn.h"

#if !defined(INCLUDE_APPEVENT_STRUCT)
#define INCLUDE_APPEVENT_STRUCT

/* ========== Union ========== */

typedef struct AppEvent_S AppEvent;

/* ========== Variables ========== */

/* Enum Event type */
enum ESS_EVENTS_TYPES {
    TOOLS_CURSOR_POSITION,
    TOOLS_CURSOR_MEASUREMENT,
    GRID_MOVE,
    GRID_ZOOM_WHEEL,
    NONE
};

/* Enum Event Button type */
enum ESS_EVENTS_BTN_TYPES {
    TOOLS_WINDOW,
    GRID_CLICK,
    GRID_CHARGE_ADD_FIXE,
    GRID_CHARGE_ADD_MOBILE,
    GRID_CHARGE_DELETE,
    GRID_MOVE_CHARGE,
    MENU_GRID_CLEAR,
    MENU_GRID_CENTER,
    MENU_SELECT_SETTINGS,
    MENU_SELECT_HELP,
    MENU_CURSOR_MEASUREMENT,
    MENU_CURSOR_DEFAULT,
    MENU_CHARGE_ADD_FIXE,
    MENU_CHARGE_ADD_MOBILE,
    MENU_CHARGE_ADD_RANDOM,  
    MENU_CHARGE_EDIT,  
    MENU_CHARGE_DELETE,
    MENU_SIMULATION_PLAY,  
    MENU_SIMULATION_PAUSE, 
    MENU_SIMULATION_STOP,  
    MENU_SIMULATION_REPLAY,
    MENU_SIMULATION_SPEEDUP,
    MENU_SIMULATION_SPEEDDOWN,
    NONE_BTN
};

/* ========== Structures ========== */

struct AppEvent_S {
    App                         *ESSApp;
    enum ESS_EVENTS_TYPES       eventType;
    enum ESS_EVENTS_BTN_TYPES   eventTypeBtn;
};

/* ========== Prototype of functions ========== */

AppEvent    *newEvent       (App *eventApp, enum ESS_EVENTS_TYPES eventType);                       /* Create event */
AppEvent    *newEventBtn    (App *eventApp, enum ESS_EVENTS_BTN_TYPES eventTypeBtn);                /* Create event btn */

gboolean    ESSEventDraw    (GtkWidget *GWidget, cairo_t *cr, App *ESSApp);                         /* Events - Draw */

gboolean    ESSEvent        (GtkWidget *GWidget, GdkEvent *GEvent, AppEvent *ESSEvent);             /* Events */
gboolean    ESSEventBtn     (GtkWidget *GWidget, GdkEventButton *GEventBtn, AppEvent *ESSEvent);    /* Events - Button */

#endif /* INCLUDE_APPEVENT_STRUCT */
