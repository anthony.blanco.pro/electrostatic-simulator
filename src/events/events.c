/* File events.c */
/* Date: 2020-06-17 */
/* Description: Event Manager */

#include "events.h"

AppEvent* newEvent(App *eventApp, enum ESS_EVENTS_TYPES eventType) {
    AppEvent* event = NEW(AppEvent);
    event->ESSApp       = eventApp;
    event->eventType    = eventType;
	event->eventTypeBtn	= NONE_BTN;
    return event;
}

AppEvent* newEventBtn(App *eventApp, enum ESS_EVENTS_BTN_TYPES eventTypeBtn) {
    AppEvent* event = NEW(AppEvent);
    event->ESSApp       = eventApp;
    event->eventType    = NONE;
	event->eventTypeBtn	= eventTypeBtn;
    return event;
}

/*
	Function: ESSEventDraw
	----------------
	Description: Drawing Event entry point
	Return: gboolean
 */
gboolean ESSEventDraw(GtkWidget *GWidget, cairo_t *cr, App *ESSApp) {
	return on_event_tools_draw(GWidget, cr, ESSApp);
}

/*
	Function: ESSEvent
	----------------
	Description: Global Events entry point
	Return: gboolean
 */
gboolean ESSEvent(GtkWidget *GWidget, GdkEvent *GEvent, AppEvent *ESSEvent) {

	switch (ESSEvent->eventType) {
		case TOOLS_CURSOR_POSITION:
			return on_event_tools_cursor_position(GWidget, GEvent, ESSEvent->ESSApp);
			break;
		case TOOLS_CURSOR_MEASUREMENT:
			return on_event_tools_cursor_measurement(GWidget, GEvent, ESSEvent->ESSApp);
			break;
		case GRID_MOVE:
			return on_event_grid_move(GWidget, GEvent, ESSEvent->ESSApp);
			break;
		case GRID_ZOOM_WHEEL:
			return on_event_grid_zoom_wheel(GWidget, GEvent, ESSEvent->ESSApp);
			break;
		case NONE:
			break;
    }

	return GDK_EVENT_STOP;

}

/*
	Function: ESSEventBtn
	----------------
	Description: Button Events entry point
	Return: gboolean
 */
gboolean ESSEventBtn(GtkWidget *GWidget, GdkEventButton *GEventBtn, AppEvent *ESSEvent) {

	switch (ESSEvent->eventTypeBtn) {
		case TOOLS_WINDOW:
			return on_event_tools_move_window(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case GRID_CLICK:
			on_event_grid_click(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case GRID_CHARGE_ADD_FIXE:
			return on_event_grid_charge_add_fixe(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case GRID_CHARGE_ADD_MOBILE:
			return on_event_grid_charge_add_mobile(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case GRID_CHARGE_DELETE:
			return on_event_grid_charge_delete(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case GRID_MOVE_CHARGE:
			return on_event_grid_move_charge(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_GRID_CLEAR:
			return on_event_btn_reset(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_GRID_CENTER:
			return on_event_btn_center(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SELECT_SETTINGS:
			return on_event_btn_settings(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SELECT_HELP:
			return on_event_btn_help(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CURSOR_MEASUREMENT:
			return on_event_btn_cursor_measurement(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CURSOR_DEFAULT:
			return on_event_btn_cursor_default(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CHARGE_ADD_FIXE:
			return on_event_btn_charge_add_fixe(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CHARGE_ADD_MOBILE:
			return on_event_btn_charge_add_mobile(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CHARGE_ADD_RANDOM:
			return on_event_btn_charge_add_random(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CHARGE_EDIT:
			return on_event_btn_charge_edit(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_CHARGE_DELETE:
			return on_event_btn_charge_delete(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_PLAY:
			return on_event_btn_play(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_PAUSE:
			return on_event_btn_pause(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_STOP:
			return on_event_btn_stop(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_REPLAY:
			return on_event_btn_replay(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_SPEEDUP:
			return on_event_btn_speedup(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
		case MENU_SIMULATION_SPEEDDOWN:
			return on_event_btn_speeddown(GWidget, GEventBtn, ESSEvent->ESSApp);
			break;
	
		case NONE_BTN:
				break;
	}

	return GDK_EVENT_STOP;

}
