# Events

1. [Introduction](#Introduction)

2. [Events](#Events)

3. [EventsBtn](#Btn)

4. [EventsGrid](#Grid)

5. [EventsTools](#Tools)

## Introduction <a name="Introduction"></a>

Le dossier **Events** contient les fichiers permettant de gérer tous les événements de l'interface.

## Events <a name="Events"></a>

| Type     | Fonction     | Description                             |
|----------|--------------|-----------------------------------------|
| AppEvent | newEvent     | Créer un nouvel événement.              |
| AppEvent | newEventBtn  | Créer un nouveau bouton événement.      |
| gboolean | ESSEventDraw | Point d'entrée de l'événement dessiner. |
| gboolean | ESSEvent     | Point d'entrée d'un événement global.   |
| gboolean | ESSEventBtn  | Point d'entrée de l'événement bouton.   |

## EventsBtn <a name="Btn"></a>

| Type     | Fonction                          | Description                                   |
|----------|-----------------------------------|-----------------------------------------------|
| gboolean | on_event_btn_center               | Centre la grille.                             |
| gboolean | on_event_btn_zoom_in              | Zoom sur la grille.                           |
| gboolean | on_event_btn_zoom_out             | Dézoom sur la grille.                         |
| gboolean | on_event_btn_reset                | Remets à zéro l'interface.                    |
| gboolean | on_event_btn_cursor_measurement   | Change pour avoir le curseur de mesure.       |
| gboolean | on_event_btn_cursor_default       | Permet de récupérer le curseur par défaut.    |
| gboolean | on_event_btn_settings             | Ouvre la fenêtre de configuration.            |
| gboolean | on_event_btn_help                 | Bouton d'aide.                                |
| gboolean | on_event_btn_charge_add_fixe      | Permet d'ajouter une charge fixe.             |
| gboolean | on_event_btn_charge_add_mobile    | Permet d'ajouter une charge mobile.           |
| gboolean | on_event_btn_charge_random        | Permet d'ajouter une charge aléatoire.        |
| gboolean | on_event_btn_charge_delete        | Permet de supprimer une charge sur la grille. |
| gboolean | on_event_btn_play                 | Lance la simulation.                          |
| gboolean | on_event_btn_pause                | Mets en pause la simulation.                  |
| gboolean | on_event_btn_stop                 | arrête la simulation.                         |
| gboolean | on_event_btn_replay               | Redémarre la simulation.                      |
| gboolean | on_event_btn_speedup              | Accélère la simulation                        |
| gboolean | on_event_btn_speeddown            | Ralentie la simulation.                       |

## EventsGrid <a name="Grid"></a>

| Type     | Fonction                        | Description                                      |
|----------|---------------------------------|--------------------------------------------------|
| gboolean | on_event_grid_zoom_wheel        | Zoom ou dézoom grâce a la roulette de la souris. |
| gboolean | on_event_grid_move              | Déplace la grille.                               |
| gboolean | on_event_grid_move_charge       | Déplace une charge sur la grille.                |
| gboolean | on_event_grid_charge_add_fixe   | Ajoute une charge fixe sur la grille.            |
| gboolean | on_event_grid_charge_add_mobile | Ajoute une charge mobile sur la grille.          |
| gboolean | on_event_grid_charge_delete     | Supprime une charge sur la grille.               |

## EventsTools <a name="Tools"></a>

| Type     | Fonction                          | Description                                                                          |
|----------|-----------------------------------|--------------------------------------------------------------------------------------|
| gboolean | on_event_tools_draw               | Dessine la fenêtre ESSAppsur l'écran.                                                |
| gboolean | on_event_tools_move_window        | Déplace la grille avec le clic droit.                                                |
| gboolean | on_event_tools_cursor_position    | Affiche les position du curseur sur la grille en modifiant le label en bas à gauche. |
| gboolean | on_event_tools_cursor_measurement | Affichele les mesures en remplaçant le label à droite de la grille.                  |