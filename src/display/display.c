/* File display.c */
/* Date: 2020-06-17 */
/* Description: Display functions */

#include "display.h"


/* Get all charges into list except idx of a charge */
List *getAllCharges(App* ESSApp, INT idx){

    List *chgs = newList();
    AppCharge* appChgBcl;
    Charge *chgBcl;
    for (INT jdx = 0; jdx < ESSApp->charges->length; jdx++) {
        if (idx != jdx) {
            appChgBcl = ESSApp->charges->GetDataByIndex(ESSApp->charges, jdx);
            chgBcl = appChgBcl->chg;
            chgs->Append(chgs, chgBcl);
        }
    }
    return chgs;

}


/* set cursor with CursorType */
VOID set_cursor(GtkWidget *widget, GdkCursorType type) {

	GdkCursor *cursor = gdk_cursor_new_for_display(gdk_display_get_default(), type);
	gdk_window_set_cursor(gtk_widget_get_window(widget), cursor);

}

/* set cursor to default */
VOID set_cursor_default(App *ESSApp, BOOLEAN restMode, BOOLEAN unlockGrid, BOOLEAN unSelectedCharge) {

	GdkCursor *cursor = gdk_cursor_new_from_name(gdk_display_get_default(), "default");
	gdk_window_set_cursor(gtk_widget_get_window(ESSApp->widgets->first->data), cursor);

	if (restMode)
		ESSApp->mode = DEFAULT;

	if (unlockGrid)
		ESSApp->isLocked = FALSE;

	if (unSelectedCharge)
		ESSApp->selectedCharge = NULL;

}

/* Display chages characteristics */
VOID set_label_characterisics_charges(App *ESSApp) {
	
	if (ESSApp->selectedCharge != NULL) {
		CHAR arr[50];
		Charge *schrg = ESSApp->selectedCharge->chg;

		// /* chrgX label */
		sprintf(arr, "x: %.lf", schrg->coord->x);
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 4), arr);

		/* chrgY label */
		sprintf(arr, "y: %.lf", schrg->coord->y);
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 5), arr);

		/* chrgM label */
		sprintf(arr, " : %e", schrg->mass);
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 6), arr);
		
		/* chrgC label */
		sprintf(arr, " : %e", ESSApp->selectedCharge->chg->chgq);
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 7), arr);
		
		/* Velocity label */
		if (ESSApp->selectedCharge->initSpeed != NULL) {
			/* chrgVX label */
			sprintf(arr, "v_x: %e", ESSApp->selectedCharge->initSpeed->x);
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 8), arr);

			/* chrgVY label */
			sprintf(arr, "v_y: %e", ESSApp->selectedCharge->initSpeed->y);
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 9), arr);
		} else {
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 8), ": ?");
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 9), ": ?");
		}

		/* Acceleration label */
		if (ESSApp->selectedCharge->acc != NULL) {
			/* chrgAX label */
			sprintf(arr, "a_x: %e", ESSApp->selectedCharge->acc->x);
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 10), arr);

			/* chrgAY label */
			sprintf(arr, "a_y: %e", ESSApp->selectedCharge->acc->y);
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 11), arr);
		} else {
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 10), ": ?");
			gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 11), ": ?");
		}

	} else {
		
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 4), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 5), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 6), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 6), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 7), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 8), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 9), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 10), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 11), ": ?");

		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 14), ": ?");
		gtk_label_set_text(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 15), ": ?");

	}

}

/* Display forces characteristics */
VOID set_label_characterisics_forces(App *ESSApp) {

	CHAR arr[300];
	
	if (ESSApp->selectedCharge != NULL && ESSApp->charges->length >= 2) {

		INT idx = atoi(gtk_label_get_text(ESSApp->selectedCharge->idx));
		Vector *coulombVector = ESSApp->selectedCharge->chg->coulombLawMultiple(getAllCharges(ESSApp, idx), ESSApp->selectedCharge->chg, ESSApp->options->scaleSize);

		/* coulombX label */
		sprintf(arr, "x: %e", coulombVector->x);
		gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 12)), arr);

		/* coulombY label */
		sprintf(arr, "y: %e", coulombVector->y);	
		gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 13)), arr);

		DELETE(coulombVector);

	} else {

		gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 12)), ": ?");
		gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 13)), ": ?");
		
	}
	

}

/* Display electrics characteristics */
VOID set_label_characterisics_electrics(App *ESSApp, DOUBLE potential, DOUBLE intensity) {
	
	CHAR arr[20];
	BOOLEAN isChargeSelected = (ESSApp->selectedCharge != NULL);
	
    sprintf(arr, "%.2e N/C", potential);
    gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 14)), arr);

    sprintf(arr, "%.2e V    ", intensity);
    gtk_label_set_text(GTK_LABEL(ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 15)), arr);
	
}
