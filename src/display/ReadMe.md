# Display

1. [Introduction](#Introduction)

2. [Display](#Display)


## Introduction <a name="Introduction"></a>

Le dossier **Display** contient les fichiers _display_ qui contiennent les fonctions permettant d'afficher des éléments sur l'interface.

## Display <a name="Display"></a>

| Type | Fonction                            | Description                                                    |
|------|-------------------------------------|----------------------------------------------------------------|
| VOID | set_cursor                          | Change le type du curseur.                                     |
| VOID | set_cursor_default                  | Change le type de curseur pour remettre le curseur par défaut. |
| VOID | set_label_characteristics_charges   | Affiche les caractéristiques de la charge.                     |
| VOID | set_label_characteristics_forces    | Affiche les caractéristiques de la force de la charge.         |
| VOID | set_label_characteristics_electrics | Affiche les caractéristiques du potentiel électrique.          |