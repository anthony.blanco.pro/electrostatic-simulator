/* File popupOptions.h */
/* Date: 2020-06-14 */
/* Description: popup edit */

#include "../app/app.h"
#include "../../libs/physics/src/movement/movement.h"

VOID on_popup_edit_clicked(GtkWidget *widget, App *ESSApp);
