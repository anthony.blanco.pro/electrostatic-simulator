/* File popupCharges.c */
/* Date: 2020-06-10 */
/* Description: popup charge */

#include "popupCharges.h"

#define _ESS_LOGS_ADD_CHARGE_FIXE_		"Adding a new negative charge on the grid."
#define _ESS_LOGS_ADD_CHARGE_MOBILE_	"Adding a new positive charge on the grid."

VOID on_destroy (GtkWidget *GWidget, App *ESSApp) {

	AppCharge *chrg = ESSApp->charges->GetLastData(ESSApp->charges);
	if (chrg->chg->chgq == 0) {
		gtk_widget_destroy(gtk_widget_get_parent(GTK_WIDGET(chrg->posX)));
		ESSApp->charges->DeleteByIndex(ESSApp->charges, ESSApp->charges->length - 1);
	}
	ESSApp->isLocked = FALSE;
	gtk_window_close(GTK_WINDOW(gtk_widget_get_toplevel(GWidget)));
}

VOID on_choice_click(GtkWidget *widget, App *ESSApp, BOOLEAN negative) {
	
	List *charges = ESSApp->charges;
	AppCharge *appChg = ESSApp->charges->GetLastData(charges);
	
	appChg->chg->chgq = 1.602E-19;
	appChg->chg->chgq = negative ? -appChg->chg->chgq : appChg->chg->chgq;
	appChg->chg->mass = negative ? ELECTRON_MASS : PROTON_MASS;

	on_destroy(widget, ESSApp);
}

gboolean on_choice_click_negative(GtkWidget *widget, GdkEvent *event, App *ESSApp) {
	on_choice_click(widget, ESSApp, TRUE);
	loggerAddLog(ESSApp->logger, INFO, _ESS_LOGS_ADD_CHARGE_FIXE_);
	return GDK_EVENT_PROPAGATE;
}

gboolean on_choice_click_positive(GtkWidget *widget, GdkEvent *event, App *ESSApp) {
	on_choice_click(widget, ESSApp, FALSE);
	loggerAddLog(ESSApp->logger, INFO, _ESS_LOGS_ADD_CHARGE_MOBILE_);
	return GDK_EVENT_PROPAGATE;
}

VOID on_popup_charges_clicked(GtkWidget *widget, App *ESSApp) {

	GtkBuilder *builder;
	GtkWidget *popup_window_charges;
	GtkWidget *buttonNegative, *buttonPositive;

	/* ========== Init GTK ========== */
	INT status;
	builder = gtk_builder_new();
	status = gtk_builder_add_from_file(builder, _ESS_GLADE_POPUP_CHARGES_, NULL);
	if (status == 0) {
		printf("ERROR : %s not found !", _ESS_GLADE_POPUP_CHARGES_);
		gtk_main_quit();
	}
	popup_window_charges = GTK_WIDGET(gtk_builder_get_object(builder, "popup_charges"));
	gtk_window_set_transient_for(GTK_WINDOW(popup_window_charges), GTK_WINDOW(ESSApp->widgets->first->data));
	g_signal_connect(G_OBJECT(popup_window_charges), "destroy", G_CALLBACK(on_destroy), ESSApp);


	buttonNegative = GTK_WIDGET(gtk_builder_get_object(builder, "popup_content_button_negative"));
	buttonPositive = GTK_WIDGET(gtk_builder_get_object(builder, "popup_content_button_positive"));
	
	g_signal_connect(G_OBJECT(buttonNegative), "button-press-event", G_CALLBACK(on_choice_click_negative), ESSApp);
	g_signal_connect(G_OBJECT(buttonPositive), "button-press-event", G_CALLBACK(on_choice_click_positive), ESSApp);
	
	gtk_widget_show_all(popup_window_charges);
	
}
