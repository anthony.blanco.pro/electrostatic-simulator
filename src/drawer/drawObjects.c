/* File drawParts.c */
/* Date: 2020-06-17 */
/* Description: Individual drawing parts */

#include "drawObjects.h"

/*
  Function: draw_circle
  ----------------
  Description:  draw a circle
  Return:
 */
VOID draw_circle(cairo_t *cr, App *app, Point *pt, DOUBLE radius, BOOLEAN fixe, BOOLEAN fill, BOOLEAN border, RGB color, RGB borderColor) {

    DOUBLE posX = (app->options->posAxeX + pt->x * app->options->zoom);
    DOUBLE posY = (app->options->posAxeY- pt->y * app->options->zoom);

    cairo_save(cr);
    cairo_set_line_width(cr, 2);
    cairo_translate(cr, posX, posY);

    if (border) {
        cairo_arc(cr, 0, 0, radius + 2, 0, 2 * G_PI);
        cairo_set_source_rgb(cr, borderColor.r / 255, borderColor.g / 255, borderColor.b / 255);
        if (fill) {
            cairo_fill(cr);
        }
    }

    cairo_arc(cr, 0, 0, radius, 0, 2 * G_PI);
    cairo_set_source_rgb(cr, color.r / 255, color.g / 255, color.b / 255);
    if (fill) {
        cairo_fill(cr);
    }
    cairo_restore(cr);
    
    if (fixe) {
        cairo_set_line_width(cr, 1);
        cairo_set_dash(cr, NULL, 0, 0);
        cairo_move_to(cr, posX, posY - radius);
        cairo_line_to(cr, posX, posY + radius);
        cairo_set_source_rgb(cr, 255.0 / 255, 255.0 / 255, 255.0 / 255);
        cairo_stroke(cr);
        cairo_move_to(cr, posX - radius, posY);
        cairo_line_to(cr, posX + radius, posY);
        cairo_set_source_rgb(cr, 255.0 / 255, 255.0 / 255, 255.0 / 255);
        cairo_stroke(cr);
    }

}

/*
  Function: draw_line
  ----------------
  Description: draw line
  Return:
 */
VOID draw_line(cairo_t *cr, DOUBLE lineWidth, const DOUBLE *dashed, RGB color, DOUBLE posX, DOUBLE posY, DOUBLE width, DOUBLE height) {
    
    cairo_set_line_width(cr, lineWidth);
    
    if (dashed != NULL) {
        cairo_set_dash(cr, dashed, 1, 0);
    }

    if (posY != -1) {
        cairo_move_to(cr, posY, 0.0);
        cairo_line_to(cr, posY, height);
    }

    if (posX != -1) {
        cairo_move_to(cr, 0.0, posX);
        cairo_line_to(cr, width, posX);
    }

    cairo_set_source_rgb(cr, color.r / 255, color.g / 255, color.b / 255);
    cairo_stroke(cr);

}
