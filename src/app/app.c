/* File app.h */
/* Date: 2020-06-17 */
/* Description: Application object functions */

#include <stdio.h>
#include <stdlib.h>

#include "app.h"

/*
  Function: addCharges
  ----------------
  Description: Add charge in App
  Return:
 */
VOID addCharges(App *self, Charge *chrg) {
  self->charges->Append(self->charges, chrg);
}

/*
  Function: addWidgets
  ----------------
  Description: Add widget in App
  Return: 
 */
VOID addWidgets(App *self, GtkWidget *GWidget) {
  self->widgets->Append(self->widgets, GWidget);
}

/*
	Function: checkPopupSettings
	----------------
	Description: Check is options input is valid 
	Return: BOOLEAN
 */
BOOLEAN checkPopupSettings(Popup *ESSpopup) {
	
	/* 2  : simTimeStart */
	/* 3  : simTimeIncrement */ 
	
	BOOLEAN isValid = TRUE;

	if (gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 2))) == (STRING)"") {
		isValid = FALSE;
	}
	if (gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 3))) == (STRING)"") {
		isValid = FALSE;
	}
	
	return isValid;
	
}

/*
  Function: savePopupSync
  ----------------
  Description: Save options input
  Return: 
 */
VOID savePopupSettings(Popup *ESSpopup) {

	/* 0  : winSizeWidth */
	/* 1  : winSizeHeight */ 
	/* 4  : spinButtonBR */
	/* 5  : spinButtonBG */
	/* 6  : spinButtonBB */
	/* 7  : spinButtonAR */
	/* 8  : spinButtonAG */
	/* 9  : spinButtonAB */
	/* 10 : spinButtonLR */
	/* 11 : spinButtonLG */
	/* 12 : spinButtonLB */

	if (gtk_spin_button_get_value(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 0))) != (DOUBLE)(ESSpopup->parentApp->options->winWidth)) {
		ESSpopup->parentApp->options->winWidth = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 0)));
	}
	if (gtk_spin_button_get_value(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 1))) != (DOUBLE)(ESSpopup->parentApp->options->winHeight)) {
		ESSpopup->parentApp->options->winHeight = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 1)));
	}

	ESSpopup->parentApp->options->simTimeStop = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 2))));
	ESSpopup->parentApp->options->simTimeIncrement = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 3))));
	
	ESSpopup->parentApp->options->colorBg.r = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 4)));
	ESSpopup->parentApp->options->colorBg.g = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 5)));
	ESSpopup->parentApp->options->colorBg.b = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 6)));

	ESSpopup->parentApp->options->colorAxes.r = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 7)));
	ESSpopup->parentApp->options->colorAxes.g = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 8)));
	ESSpopup->parentApp->options->colorAxes.b = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 9)));

	ESSpopup->parentApp->options->colorLines.r = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 10)));
	ESSpopup->parentApp->options->colorLines.g = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 11)));
	ESSpopup->parentApp->options->colorLines.b = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 12)));

	ESSpopup->parentApp->isSettingChange = TRUE;

}

/*
  Function: savePopupEdit
  ----------------
  Description: Save options input
  Return: 
 */
VOID savePopupEdit(Popup *ESSpopup) {

	/* 0  : entryCharge */
	/* 1  : entryMass */ 
	/* 2  : entryPosX */
	/* 3  : entryPosY */
	/* 4  : checkFixe */
	/* 5  : entrySpdX */
	/* 6  : entrySpdY */

	ESSpopup->parentApp->selectedCharge->chg->chgq = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 0))));
	ESSpopup->parentApp->selectedCharge->chg->mass = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 1))));
	ESSpopup->parentApp->selectedCharge->chg->coord->x = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 2))));
	ESSpopup->parentApp->selectedCharge->chg->coord->y = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 3))));
	ESSpopup->parentApp->selectedCharge->chg->fixe = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 4)));
	if(ESSpopup->parentApp->selectedCharge->initSpeed == NULL) {
		ESSpopup->parentApp->selectedCharge->initSpeed = newVector(0, 0, 0);
	}
	ESSpopup->parentApp->selectedCharge->initSpeed->x = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 5))));
	ESSpopup->parentApp->selectedCharge->initSpeed->y = atof(gtk_entry_get_text(GTK_ENTRY(ESSpopup->widgets->GetDataByIndex(ESSpopup->widgets, 6))));

}

/*
	Function: displayLogs
	----------------
	Description: Display logs in app
	Return:
 */
VOID displayLogs(App *ESSApp) {
	
	List *logs = ESSApp->logger->logs;
	INT idx = logs->length;
	
	if (idx != 0) {
		GtkWidget *logBox = ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 19);
		while (idx != 0) {
			GtkWidget *logLabel = GTK_WIDGET(gtk_label_new((STRING)(logs->GetDataByIndex(logs, idx - 1))));
			gtk_label_set_xalign(GTK_LABEL(logLabel), 0);
			gtk_label_set_yalign(GTK_LABEL(logLabel), 0);
			gtk_container_add(GTK_CONTAINER(logBox), logLabel);
			gtk_box_reorder_child(GTK_BOX(logBox), GTK_WIDGET(logLabel), 0);
			idx--;
		}
		logs->Purge(logs);
		gtk_widget_show_all(logBox);
	}
	
}

/*
	Function: changeBtnStatus
	----------------
	Description: Change buttons status dynamicaly
	Return: gboolean
 */
VOID changeBtnStatus(App *ESSApp) {
	
	switch (ESSApp->simStatus) {
		case RUNING:
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 20), FALSE);			/* Lock play btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 21), TRUE);			/* Lock pause btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 22), TRUE);			/* Lock stop btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 23), TRUE);			/* Lock replay btn */
			break;

		case PAUSED:
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 20), TRUE);			/* Lock play btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 21), TRUE);			/* Lock pause btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 22), TRUE);			/* Lock stop btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 23), TRUE);			/* Lock replay btn */
			break;

		case STOPED:
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 20), TRUE);			/* Lock play btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 21), FALSE);			/* Lock pause btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 22), FALSE);			/* Lock stop btn */
			gtk_widget_set_sensitive (ESSApp->widgets->GetDataByIndex(ESSApp->widgets, 23), FALSE);			/* Lock replay btn */
			break;
	}

}

/*
  Function: newApp
  ----------------
  Description: Create new app object
  Return: App*
 */
App *newApp(GtkApplication *GApp, GtkBuilder *GBuilder) {
	App *app = NEW(App);

	app->GtkApp         = GApp;
	app->Builder        = GBuilder;

	app->widgets        = newList();
	app->charges        = newList();
	app->saveCharges    = NULL;

	app->selectedCharge = NULL;
	app->logger         = newLogger();
	app->options        = newAppOption();

	app->isSettingChange = FALSE;
	app->isLocked       = FALSE;

	app->mode           = DEFAULT;
	app->simStatus 		= STOPED;

	app->addCharges     = addCharges;
	app->addWidgets     = addWidgets;

	return app;
}

/*
  Function: Create newPopup
  ----------------
  Description: Create new popup object
  Return: Popup*
 */
Popup *newPopup(App *parentApp, List *widgets, List *extraArgs) {
  Popup *popup = NEW(Popup);

  popup->parentApp = parentApp;
  popup->widgets = widgets;
  popup->extraArgs = extraArgs;

  return popup;
}