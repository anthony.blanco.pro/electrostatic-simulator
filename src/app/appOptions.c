/* File appOptions.c */
/* Date: 2020-06-19 */
/* Description: App Option functions */

#include "appOptions.h"

/*
  Function: newAppOptions
  ----------------
  Description: Create a new AppOption
  Return: AppOption
 */
AppOption *newAppOption() {
    AppOption *option = NEW(AppOption);

    option->winWidth = 1000;
    option->winHeight = 800;
    option->areaWidth = option->winWidth * _ESS_WINDOW_RATIO_;
    option->areaHeight = option->winHeight * _ESS_WINDOW_RATIO_;
    
    option->zoom = 1;
    option->zoomGraphic = 20;
    option->scaleSize = 1E-9;

    option->simTimeStart = 0;
    option->simTimeStop = 10E99;
    option->simTimeIncrement = 1E-12;
    
    option->colorBg = (RGB){0, 0, 0};
    option->colorAxes = (RGB){255, 255, 0};
    option->colorLines = (RGB){255, 255, 255};
    option->colorChargeNeg = (RGB){0, 0, 255};
    option->colorChargePos = (RGB){255, 0, 0};
    option->colorChargeBorder = (RGB){0, 255, 0};

    return option;
}