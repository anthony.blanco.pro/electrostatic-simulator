/* File appCharge.c */
/* Date: 2020-06-11 */
/* Description: Application charge with labels */

#include "appCharge.h"

/*
  Function: newAppCharge
  ----------------
  Description: Create a new appCharge
  Return: AppCharge
 */
AppCharge *newAppCharge(Charge *chg, GtkLabel *idx, GtkLabel *posX, GtkLabel *posY) {
    AppCharge *app = NEW(AppCharge);
    app->chg = chg;
    app->acc = 0;
    app->initSpeed = 0;
    app->idx = idx;
    app->posX = posX;
    app->posY = posY;

    return app;
}
