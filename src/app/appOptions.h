/* File appextra.h */
/* Date: 2020-06-16 */
/* Description: Application object extra */

/* ========== Includes ========== */

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>

#include "../../libs/physics/src/movement/movement.h"
#include "../../utils/RGB.h"
#include "../../utils/MACRO_REF.h"

#if !defined(INCLUDE_APPOPTION_STRUCT)
#define INCLUDE_APPOPTION_STRUCT

/* Window const */
#define _ESS_WINDOW_ZOOM_COEF_  1.2
#define _ESS_WINDOW_RATIO_      0.65

/* ========== Union ========== */

typedef struct AppOption_S AppOption;


/* ========== Structure ========== */

struct AppOption_S {
    /* Window variables */
    INT         winWidth;
    INT         winHeight;
    INT         areaWidth;
    INT         areaHeight;

    DOUBLE      posAxeX;
    DOUBLE      posAxeY;
    DOUBLE      zoom;
    DOUBLE      zoomGraphic;
    DOUBLE      scaleSize;

    DOUBLE      posX;
    DOUBLE      posY;

    /* Simulations variables */
    DOUBLE      simTimeStart;
    DOUBLE      simTimeStop;
    DOUBLE      simTimeIncrement;

    /* Colors variables */
    RGB         colorBg;
    RGB         colorAxes;
    RGB         colorLines;
    RGB         colorChargePos;
    RGB         colorChargeNeg;
    RGB         colorChargeBorder;
};

/* ========== Prototype of functions ========== */

AppOption *newAppOption();

#endif /* INCLUDE_APPOPTION_STRUCT */
