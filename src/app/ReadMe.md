# App

1. [Introduction](#Introduction)

2. [App](#App)

3. [AppCharge](#Charge)

4. [AppLogger](#Logger)

5. [AppOptions](#Options)

## Introduction <a name="Introduction"></a>

Le dossier **App** contient les fichiers permettant de travailler sur l'application.

## App <a name="App"></a>

| Define                    | Description                      |
|---------------------------|----------------------------------|
| _ESS_APP_VERSION_         | "v1.0.0"                         |
| _ESS_WINDOW_ZOOM_COEF_    | 1.2                              |
| _ESS_WINDOW_RATIO_        | 0.65                             |
| _ESS_CHARGE_RADIUS_       | 10                               |
| _ESS_GLADE_APP_           | "ui/ess_gui_app.glade"           |
| _ESS_GLADE_POPUP_OPTIONS_ | "ui/ess_gui_popup_options.glade" |
| _ESS_GLADE_POPUP_CHARGES_ | "ui/ess_gui_popup_charges.glade" |


| Type  | Fonction          | Description                                    |
|-------|-------------------|------------------------------------------------|
| VOID  | addCharges        | Ajoute une charge dans l'application.          |
| VOID  | addWidgets        | Ajoute un widget dans l'application.           |
| VOID  | checkPopupOptions | Vérifie si les options d'entrées sont valides. |
| VOID  | savePopupOptions  | Sauvegarde les options d'entrées.              |
| App   | newApp            | Créer un nouvel objet App.                     |
| Popup | newPopup          | Créer un nouvel objet Popup.                   |

## AppCharge <a name="Charge"></a>

| Type      | Fonction     | Description                |
|-----------|--------------|----------------------------|
| AppCharge | newAppCharge | Créer une nouvelle charge. |

## AppLogger <a name="Logger"></a>

| Type      | Fonction  | Description                                   |
|-----------|-----------|-----------------------------------------------|
| AppLogger | newLogger | Créer un nouveau logger.                      |
| VOID      | ESSLog    | Renvoie un message en fonction du type voulu. |

## AppOptions <a name="Options"></a>

| Type      | Fonction     | Description                                 |
|-----------|--------------|---------------------------------------------|
| AppOption | newAppOption | Génère toutes les options de l'application. |
