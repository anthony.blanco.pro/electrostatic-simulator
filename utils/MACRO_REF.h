/** Date: 2020-02-12
 *  File: MACRO_REF.h
 *  Desc: Base lib ref file
*/

#include <gtk/gtk.h>

#pragma once
#ifndef MOD_REF

    #define MOD_REF
    
    #define NEW(type)           ((type*)malloc(sizeof(type)))
    #define DELETE(ptr)         (free(ptr))
    #ifndef NULL
        #define NULL (void*)0
    #endif

    #define FUNC(fnc)           (*fnc)
    #define CASTV(var, type)    (*(type*)var)
    #define FLAGS(num)           (printf("Flag #%d\n",num))            /* DEBUG ONLY */

    #define UNUSED(x) (void)(x)

    #ifndef FALSE
        #define FALSE 0
    #endif

    #ifndef TRUE
        #define TRUE 1
    #endif

    typedef void            VOID;

    typedef signed char     INT8;
    typedef signed short    INT16;
    typedef signed int      INT32;
    typedef signed long     INT64;

    typedef unsigned char   UINT8;
    typedef unsigned short  UINT16;
    typedef unsigned int    UINT32;
    typedef unsigned long   UINT64;

    typedef unsigned char   UCHAR;
    typedef unsigned short  USHORT;
    typedef unsigned int    UINT;
    typedef unsigned long   ULONG;

    typedef int             INT;
    typedef char            CHAR;
    typedef short           SHORT;
    typedef float           FLOAT;
    typedef long            LONG;
    typedef double          DOUBLE;

    typedef CHAR*           STRING;
    typedef UCHAR           BYTE;

    typedef gboolean        BOOL;
    typedef BYTE            BOOLEAN;

#endif
