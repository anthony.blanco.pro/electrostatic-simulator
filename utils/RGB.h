/* File RGB.h */
/* Date: 2020-06-10 */
/* Description: RGB color struct */

#include "MACRO_REF.h"

#if !defined(INCLUDE_RGB_STRUCT)
#define INCLUDE_RGB_STRUCT

typedef struct RGB_s {
  DOUBLE r;
  DOUBLE g;
  DOUBLE b;
} RGB;

#endif /* INCLUDE_RGB_STRUCT */
