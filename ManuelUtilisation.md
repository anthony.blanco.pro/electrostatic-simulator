# Manuel d'utilisation

## Installation

Afin de pouvoir utiliser correctement notre application il faut installer : 

GTK 3 && pkgConfig

Pour utiliser l'application correctement il ya plusieurs commandes à savoir : 

| Commande      | Action                                                                                    |
|---------------|-------------------------------------------------------------------------------------------|
| make          | Compile l'application.                                                                    |
| make sync     | Synchronise les sub-module.                                                               |
| make launch   | Compile et lance l'application.                                                           |
| make unit     | Lance les tests unitaires.                                                                |
| make mrproper | Supprime tous les fichiers générés.                                                       |
| make clean    | Supprime tous les fichiers générés dans l'application présente sauf le fichier de config. |

Pour utiliser l'application veuillez effectuer les commandes suivantes : 

git clone <https://gitlab.com/isen-projects/info/projet-c/electrostatic-simulator.git>

cd electrostatic-simulator

make sync && make launch

## Sommaire

1. [Introduction](#Introduction)

2. [Présentation générale](#Presentation)

3. [La grille](#Grille)

4. [Les boutons](#Boutons)

5. [Les onglets](#Onglets)

## Introduction <a name="Introduction"></a>

Voici le manuel d'utilisation de notre application de simulation électrostatique. Ce manuel vous permettra de comprendre comme l'utiliser. Nous allons séparer ce manuel en plusieurs étapes : L'application en général puis un focus sur la grille, et un focus sur les boutons et leurs fonctions.

## Présentation générale <a name="Presentation"></a>

Nous avons créer une simulation électrostatique que vous pouvez lancer dans le terminal à l'aide d'un _make sync_ qui sert à actualiser les sub-module puis un _make launch_ pour compiler et lancer l'application, ensuite vous vous retrouverez face à une interface graphique. Cette interface constitue notre application. Elle permet de créer et déposer des charges sur la grille, il est ensuite possible de lancer une simulation avec le bouton _play_ pour un temps donné. Il y aura ensuite sur le coté droit toutes les informations à savoir comme la position des charges, la masse, la vitesse etc...

## La grille <a name="Grille"></a>

La grille est la partie la plus importante de notre interface c'est sur celle-ci que vous allez voir vos simulation. Les axes jaunes sont bien évidement les axes des ordonnées et des abscisses et leur croisement correspond au point de coordonnées _(0,0)_ Vous pouvez faire un clique gauche sur n'importe quel endroit de cette grille et glisser afin de la déplacer. De plus si au lieu de cliquer sur la grille, vous cliquez sur une charge vous pouvez glisser afin de déplacer la charge sur la grille, si vous cliquez sur la charge, vous aurez toutes les informations de la charge dans l'onglet à droite de la fenêtre.

Vous pouvez aussi voir en bas à gauche de la grille la position de votre curseur sous la forme _x=200 y=0_ et en bas à droite la version de notre projet.

## Les boutons <a name="Boutons"></a>

### Les boutons d'ajout d'une charge

Vous trouverez sur la gauche tous les boutons de notre interface. Tout d'abord, les 3 premiers boutons sont les boutons d'ajouts d'un charge comme décrit dans le tableau ci-dessous. Il faut savoir qu'il faut cliquer sur le bouton, choisir dans la popup si vous voulez une charge positive ou négative et ensuite cliquer sur l'endroit de la grille où vous voulez déposer la charge. Mais les boutons prennent aussi en compte le double clique, qui permet de placer une charge random. C'est à dire qu'elle sera positive ou négative mais vous ne pouvez pas choisir et qu'elle sera placé à un endroit au hasard sur la grille.


| Icône                                       | Bouton                    | Fonction                                                              |
|---------------------------------------------|---------------------------|-----------------------------------------------------------------------|
| <img src = "ui/ess_icon_chrg_fixe.png">     | Ajout d'une charge fixe   | Ajoute au choix une charge fixe positive ou négative sur la grille.   |
| <img src = "ui/ess_icon_chrg_mobile.png">   | Ajout d'une charge mobile | Ajoute au choix une charge mobile positive ou négative sur la grille. |
| <img src = "ui/ess_icon_add_chrg_rand.png"> | Ajout d'une charge random | Ajoute une charge au hasard (fixe ou mobile), positive ou négative.   |


### Les différents boutons

| Icone                                | Bouton | Fonction                               |
|--------------------------------------|--------|----------------------------------------|
| <img src = "ui/ess_icon_center.png"> | Center | Recentre la grille.                    |
| <img src = "ui/ess_icon_select.png"> | Select | Sélectionne le curseur.                |
| <img src = "ui/ess_icon_mark.png">   | Mark   | Permet d'avoir le mode mesure.         |
| <img src = "ui/ess_icon_edit.png">   | Edit   | Modifie les paramètres de la charge.   |
| <img src = "ui/ess_icon_delete.png"> | Delete | Supprime une charge en cliquant dessus |
| <img src = "ui/ess_icon_clean.png">  | Clean  | Remets la grille à zéro.               |


### Les boutons de simulation

| Icone                                      | Bouton       | Fonction                     |
|--------------------------------------------|--------------|------------------------------|
| <img src = "ui/ess_icon_play.png">         | Play         | Lance la simulation.         |
| <img src = "ui/ess_icon_pause.png">        | Pause        | Mets en pause la simulation. |
| <img src = "ui/ess_icon_stop.png">         | Stop         | Arrête la simulation.        |
| <img src = "ui/ess_icon_replay.png">       | Replay       | Relance la simulation.       |
| <img src = "ui/ess_icon_speed_up.png">     | Speed up     | Accélère la simulation       |
| <img src = "ui/ess_icon_speed_down.png">   | Speed down   | Ralentie la simulation.      |


### Les boutons de paramètres


| Icône                                  | Bouton   | Fonction                             |
|----------------------------------------|----------|--------------------------------------|
| <img src = "ui/ess_icon_settings.png"> | Settings | Ouvre la popup des paramètres.       |
| <img src = "ui/ess_icon_help.png">     | Help     | Renvoie sur le manuel d'utilisation. |


## Les onglets <a name="Onglets"></a>

Les onglets à droite de la fenêtre vous montre toutes les informations à savoir sur la simulation. Vous pouvez y trouver le nombre de charge ainsi que leurs positions. En dessous, lorsque vous cliquez sur une charge, vous pouvez y trouver toutes les informations relatives aux charges comme la masse, la position, la vitesse ou encore l'accélération. De plus dans l'onglet en dessous, il y a la loi de coulomb, le potentiel électrique et l'intensité du champ électrique.

| Onglet                   | Fonction                                                                                    |
|--------------------------|---------------------------------------------------------------------------------------------|
| Charges liste            | Contient la liste de toutes les charges et affiche leurs position en direct.                |
| Charge characteristics   | Contient les caractéristiques des charges (position, masse, charge, vitesse, accélération). |
| Force characteristics    | Contient les valeurs de la force de Coulomb sur les charges.                                |
| Electric characteristics | Contient le potentiel électrique.                                                           |